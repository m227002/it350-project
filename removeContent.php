<?php
if(!isset($_COOKIE['loggedin'])) {
  echo'<script>alert("Please Log-in First!");</script>';
  header("Location: loginPage.html");
}
$item = $_POST["item"];
$desc = $_POST["desc"];
$price = $_POST["price"];
$username = $_COOKIE['username'];
//get file to read
$file = fopen("LOG.txt", "r");
$found = false;
$firstLine = fgets($file);
//read first line that has variable names
$firstLineList = preg_split('/\t/', $firstLine);
while(!feof($file)) {
  $oneLine = fgets($file);
  //split up line into values
  $arrayString = preg_split('/\t/', $oneLine);
  //check if username is found
  if ($arrayString[3]===$username){
    $content = $arrayString[10];
    $arrayContent = preg_split('/|/', $content);

      //set cookies and redirect
      header("Location: index.html");
      //found is true as email and correct password found
      $found = true;
    }
  }
}

if (!$found){
  //redirect back to login
  echo'<script>alert("Incorrect password or username!");</script>';
  header("Location: loginPage.html");
}
?>
