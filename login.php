<?php
$username = $_POST["username"];
$password = $_POST["pass"];
//get file to read
$file = fopen("LOG.txt", "r");
$found = false;
$firstLine = fgets($file);
//read first line that has variable names
$firstLineList = preg_split('/\t/', $firstLine);
while(!feof($file)) {
  $oneLine = fgets($file);
  //split up line into values
  $arrayString = preg_split('/\t/', $oneLine);
  //check if email is found
  if ($arrayString[3]===$username){
    //check if password is correct
    if(($password === $arrayString[4])&&($arrayString[11]=="no")){
      setcookie("username", $username);
      setcookie("loggedin", "true");
      setcookie("FirstName",$arrayString[0]);
      setcookie("LastName",$arrayString[1]);
      setcookie("Email",$arrayString[2]);
      setcookie("Password",$arrayString[4]);
      setcookie("Phone",$arrayString[5]);
      setcookie("Send",$arrayString[7]);
      setcookie("Company",$arrayString[6]);
      setcookie("Admin",$arrayString[8]);
      setcookie("Friends",$arrayString[9]);
      setcookie("Content",$arrayString[10]);
      setcookie("Locked",$arrayString[11]);
      //set cookies and redirect
      header("Location: index.html");
      //found is true as email and correct password found
      $found = true;
    }
  }
}

if (!$found){
  //redirect back to login
  echo'<script>alert("Incorrect password or username!");</script>';
  header("Location: loginPage.html");
}
?>
